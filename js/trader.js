﻿
function Parse(firstName, lastName, secondLastName, bornDate, gender, email, phone, phone2) {

  var trader = new Trader(firstName, lastName, secondLastName, bornDate, gender, email, phone, phone2);
  return trader;

};

var FullTrader = function (trader, traderType, taxIDNumber, contacts, customers, suppliers) {

  this.trader = trader;
  this.traderType = traderType;
  this.taxIDNumber = taxIDNumber;
  this.contacts = contacts;
  this.customers = customers;
  this.suppliers = suppliers;

}

var Trader = function (firstName, lastName, secondLastName, bornDate, gender, email, phone, phone2) {

  this.id = Math.floor((Math.random() * 10000) + 1);
  this.firstName = firstName;
  this.lastName = lastName;
  this.secondLastName = secondLastName;
  this.bornDate = bornDate;
  this.gender = gender;
  this.typeId = 0;
  this.fullName = this.firstName + ' ' + this.lastName + ' ' + this.secondLastName;
  this.email = email;
  this.phone = phone;
  this.phone2 = phone2;
  this.addresses = [];  
  
}

function addTrader(trader) {
 
  var traders = parseJson('traders');
  var element = new FullTrader(trader, "1929", "YAPC82-02-02", [], [], []);

  traders.push(element);
  convertJson('traders',traders);
   
  return;
}

function delTrader(traderId) {

  var traders = parseJson('traders');
  for (index = 0; index < traders.length; index++) {
    if (traderId == traders[index].trader.id) {
      traders.splice(index, 1);
      convertJson('traders',traders);
      return 0;
    }

  }

  return -1;
}

function getTrader(fullName) {

  var traders = parseJson('traders');
  for (index = 0; index < traders.length; index++) {
    if (fullName == traders[index].trader.fullName) {
      return traders[index];
    }
  }

  return null;
}

function getTraderById(traderId) {

  var traderObj = parseJson('traders');
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {
      return traderObj[index];
    }
  }

  return null;
}

function getSecondLastName(secondLastName) {

  if (secondLastName.length == 0) {
    return "";
  }
  else {
    return ' ' + secondLastName;
  }

}

function updTrader(traderId, newTrader) {

  var traders = parseJson('traders');  
  for (index = 0; index < traders.length; index++) {
    if (traderId == traders[index].trader.id) {

      traders[index].trader.firstName = newTrader.firstName;
      traders[index].trader.lastName = newTrader.lastName;
      traders[index].trader.secondLastName = newTrader.secondLastName;
      traders[index].trader.bornDate = newTrader.bornDate;
      traders[index].trader.gender = newTrader.gender;
      traders[index].trader.fullName = newTrader.firstName + ' ' + newTrader.lastName + getSecondLastName(newTrader.secondLastName);
      traders[index].trader.email = newTrader.email;
      traders[index].trader.phone = newTrader.phone;
      traders[index].trader.phone2 = newTrader.phone2;
      convertJson('traders',traders);
    }
  }

  return;
}