﻿function loadJsonFile(jsonFile) {

 items = [];
  $.ajax({
    url: jsonFile,
    dataType: 'json',
    async: false,
    success: function (json) {
    items= json;
    }
  });

  return items;
}

function convertJson(sessionVariable, jsonItems) {

  sessionStorage.removeItem(sessionVariable);
  var jsonObj = JSON.stringify(jsonItems);
  sessionStorage.setItem(sessionVariable, jsonObj);

  return;
}

function parseJson(sessionVariable) {

  var jsonObjects = JSON.parse(sessionStorage.getItem(sessionVariable));
  var objs = [];
  for (var i in jsonObjects) {
     objs.push(jsonObjects[i]);
  }

  return objs;
}