﻿$(document).ready(function () {
  
  var jsonItems = loadJsonFile('json/contacts.data.json.txt');
  convertJson('traders', jsonItems)
  fillListNames();
   
});

function fillListNames() {

  traders = parseJson('traders');
  var option = document.createElement('option');
  for (i = 0; i < traders.length; i++) {
    option = document.createElement('option');
    option.innerHTML = traders[i].trader.fullName;
    document.getElementById('list-names').appendChild(option);
  }

  return;
}

function clearNamesList() {

  var list = document.getElementById('list-names').options;
  var count = list.length;

  for (i = 0; i < count; i++) {
    list[0].parentNode.removeChild(list[0]);
  }

  return;
}

function existTraderFullName(fullName) {

  if (getTrader(fullName) == null) {    
    return -1;
  }

  return 0;
}

function validateTraderFullName(fullName) {

  if (fullName.length == 0) {
    alert("Upps!!! El nombre se encuentra en blanco!!! ");
    return -1;
  }  
  if (existTraderFullName(fullName) == -1) {
    alert("El contacto con el nombre: " + fullName + " no se ecuentra en la base de datos!!!");
    return -1;
  }

  return 0;
}

function searchTrader() {

  var fullName = document.getElementById('txt-name').value;
  if (validateTraderFullName(fullName) == 0) {
    var traderObj = getTrader(fullName);
    sessionStorage.setItem("traderId", traderObj.trader.id);
    showTraderInfo(traderObj.trader.id);
  }
 
  return;
}

function showGeneralInfo(traderId) {

  var traderObj = getTraderById(traderId);
 
  $('#div-generalInfo').empty();
  var generalInfo = "Nombre: <a href='ContactEditor.aspx'>" + traderObj.trader.fullName + "</a> <br/> " +
                      "Correo electrónico: " + traderObj.trader.email + " <br/> " +
                      "Teléfono: " + traderObj.trader.phone + " <br/> " +
                      "Celular: " + traderObj.trader.phone2 + " <br/> " +
                      "Tipo: " + traderObj.traderType + "<br/>" +
                      "RFC: " + traderObj.taxIDNumber + "<br/> ";
  
  $('#div-generalInfo').append(generalInfo);
  
  return;
}

function showAddressesInfo(address,addressIndex, addresslength) {

  $('#div-addressesInfo').empty();

  var addressInfo = "Calle: " + address.street + " <br/> " +
              "No. Exterior: " + address.externalNo + "   " + "No Interior: " + address.internalNo + " <br/> " +
              "Colonia: " + address.location + " <br/> " +
              "Municipio o Delegación: " + address.municipality + " <br/> " +
              "Estado: " + address.state + "<br/>" +
              "País: " + address.country + "<br/>" +
              "C.P. " + address.zipCode + "<br/>" +
              "<hr/> " +
              " <a href='#' onclick='getLastAddress();'> Anteriror </a> " +
               addressIndex + " de " + addresslength +
              " <a href='#' onclick='getNextAddress();'> siguiente </a>" + " <br /> ";
  $('#div-addressesInfo').append(addressInfo);
  
  return;
}

function getLastAddress() {

  var addressIndex = sessionStorage.getItem("addressIndex");
  addressIndex--;
  printAddress(addressIndex);

  return;
}

function getNextAddress() {

  var addressIndex = sessionStorage.getItem("addressIndex");
  addressIndex++;
  printAddress(addressIndex);

  return;
}
function validateAddressIndex(addresses, addressIndex) {

  if (addressIndex < 0) {
    return 0;
  }

  if (addressIndex >= addresses.length) {
    return addresses.length - 1;
  }

  return addressIndex;
}

function printAddress(addressIndex) {
  
  var traderId = sessionStorage.getItem('traderId');
  var addresses = getAddresses(traderId);
  var index = validateAddressIndex(addresses,addressIndex)
  var address = addresses[index];

  sessionStorage.setItem("addressIndex", index);
  index++;
  showAddressesInfo(address,index,addresses.length);
 
  return;
}

function existAddress(traderId) {
  
  var addresses = getAddresses(traderId);
  if (addresses.length == 0) {
    $('#div-addressesInfo').empty();

    var addressInfo = "No contiene direcciones!!!<br> ";
    $('#div-addressesInfo').append(addressInfo);
    
    return;
  } else {
    var addresses = getAddresses(traderId);
    printAddress(0);
   
    return;
  }
    
}

function showTableInfo() {

  var tableInfo = document.getElementById("table-info");
  if (tableInfo.style.visibility != "visible") {
    tableInfo.style.visibility = "visible";
  }

  return;
}

function hideTableInfo() {

  var tableInfo = document.getElementById("table-info");
  if (tableInfo.style.visibility == "visible") {
    tableInfo.style.visibility = "hidden";
  }

  return;
}

function showTraderInfo(traderId) {

  showTableInfo();
  showGeneralInfo(traderId);
  existAddress(traderId);
  //showAddressesInfo
  //showSupplier
  //show Contacts
  //show Providers
}


