﻿function showAddressForm() {

  var addressForm = document.getElementById("div-address");
  if (addressForm.style.visibility != "visible") {
    addressForm.style.visibility = "visible";
    fillCountryComboBox();
    cleanAddressForm();
  }

  return;
}

function hideAddressForm() {

  var addressForm = document.getElementById("div-address");
  if (addressForm.style.visibility == "visible") {
    addressForm.style.visibility = "hidden";
  }

  return;
}
//  Combobox Operation
function fillComboBox(comboBoxName, dataList) {

  var select = document.getElementById(comboBoxName);
  for (i = 0; i < dataList.length; i++) {
    var option = document.createElement('Option');
    option.innerHTML = dataList[i].name;
    option.value = dataList[i].id;
    select.appendChild(option);
  }

  return;
}

function getComboSelectedText(comboBoxName) {

  var comboBox = document.getElementById(comboBoxName);
  var text = comboBox.options[comboBox.selectedIndex].text;

  return text;
}

function getComboSelectedId(comboBoxName) {

  var comboBox = document.getElementById(comboBoxName);
  var id = comboBox.options[comboBox.selectedIndex].value;

  return id;
}

function selectComboBySelectedIndex(comboBoxName, index) {

  document.getElementById(comboBoxName).selectedIndex = index;  

  return;
}

function cleanComboBox(comboBoxName) {

  var selectObj = document.getElementById(comboBoxName);
  var count = selectObj.options.length;
  for (i = 0; i < count - 1; i++) {
    selectObj.remove(1);
  }

  return;
}

function cleanTextBox(textBoxName) {

  var textObj = document.getElementById(textBoxName);
  textObj.value = "";
  
  return;
}

// End Combobox Operation

function cleanAddressForm() {

  cleanTextBox("txt-description");
  selectComboBySelectedIndex('cbo-country', 0);
  cleanComboBox('cbo-state');
  cleanComboBox('cbo-municipality');
  cleanComboBox('cbo-location');
  cleanTextBox("txt-street");
  cleanTextBox("txt-externalNo");
  cleanTextBox("txt-internalNo");
  cleanTextBox("txt-zipCode");

  return;
}

//get and Fill ComboBoxes
function getStates(countryId) {

  var statesObj = loadJsonFile('/json/states.data.json.txt');
  var states = new Array();

  for (i = 0; i < statesObj.state.length; i++) {
    if (statesObj.state[i].countryId == countryId) {
      states.push(statesObj.state[i]);
    }
  }

  return states;
}

function getMuncipalites(stateId) {

  var muncipalitiesObj = loadJsonFile('/json/municipalities.data.json.txt');
  var muncipalities = new Array();

  for (i = 0; i < muncipalitiesObj.municipality.length; i++) {
    if (muncipalitiesObj.municipality[i].stateId == stateId) {
      muncipalities.push(muncipalitiesObj.municipality[i]);
    }
  }

  return muncipalities;
}

function getLocations(muncipalityId) {

  var locationsObj = loadJsonFile('/json/locations.data.json.txt');
  var locations = new Array();
  for (i = 0; i < locationsObj.location.length; i++) {
    if (locationsObj.location[i].muncipalityId == muncipalityId) {
      locations.push(locationsObj.location[i]);
    }
  }

  return locations;
}

function fillCountryComboBox() {

  var countries = loadJsonFile('/json/countries.data.json.txt');
  countries = countries.country;
  cleanComboBox('cbo-country');
  fillComboBox("cbo-country", countries);

  return;
}

function fillStatesComboBox(comboboxName) {

  var countryId = getComboSelectedId('cbo-country');
  var statesList = getStates(countryId);
  cleanComboBox('cbo-state');
  cleanComboBox('cbo-municipality');
  cleanComboBox('cbo-location');
  fillComboBox("cbo-state", statesList);

  return;
}

function fillMuncipalitiesComboBox() {

  var stateId = getComboSelectedId('cbo-state');
  var municipalitiesList = getMuncipalites(stateId);
  cleanComboBox('cbo-municipality');
  cleanComboBox('cbo-location');
  fillComboBox('cbo-municipality', municipalitiesList);

  return;
}

function fillLocationsComboBox() {

  var municipalityId = getComboSelectedId('cbo-municipality');
  var lcoationsList = getLocations(municipalityId);
  cleanComboBox('cbo-location');
  fillComboBox('cbo-location', lcoationsList);

  return;
}

//get and Fill ComboBoxes


//region validate Form

function validateComboBox(comboBoxName) {

  var optionId = getComboSelectedId(comboBoxName);
  if (optionId == -1) {
    document.getElementById(comboBoxName).focus();
    return -1;
  } else {
    return 0;
  }

}

function validateEmpity(inputTextId) {

  var element = document.getElementById(inputTextId).value;

  if (element.length == 0) {
    document.getElementById(inputTextId).focus();
    return -1;
  } else {
    return 0;
  }

}

function validateNumericExpression(value) {

  var numericExpression = /^\d{5}$/;
  if (!numericExpression.test(value)) {
    return -1;
  } else {
    return 0;
  }

}

function validateZiCope() {

  if (validateEmpity('txt-zipCode') == -1) {
    alert("Aún no has escrito el código postal!!!");
    return -1;
  }

  if (validateNumericExpression(document.getElementById('txt-zipCode').value) == -1) {
    alert("El Código Postal debe tener una longitud de 5 caracteres únicamente numéricos!!!");
    document.getElementById('txt-zipCode').focus();
    return -1;
  }

  return 0;
}

function validateAddress() {

  if (validateComboBox('cbo-country') == -1) {
    alert("selecciona un país de la lista de paises!!!");
    return -1;
  }
  if (validateComboBox('cbo-state') == -1) {
    alert("selecciona un estado de la lista de estados!!!");
    return -1;
  }
  if (validateComboBox('cbo-municipality') == -1) {
    alert("selecciona un municipio de la lista de  municipios!!!");
    return -1;
  }
  if (validateComboBox('cbo-location') == -1) {
    alert("selecciona una localidad de la lista de Localidades!!!");
    return -1;
  }
  if (validateEmpity('txt-street') == -1) {
    alert("Aún no has escrito la calle!!!");
    return -1;
  }
  if (validateEmpity('txt-externalNo') == -1) {
    alert("Aún no has escrito el número exterior!!!");
    return -1;
  }
  if (validateZiCope() == -1) {
    return -1;
  }

  return 0;
}

//end region validte Form

// Address Operations

function loadAddress() {

  var id = -10;
  var description = document.getElementById("txt-description").value;
  var country = getComboSelectedText('cbo-country');
  var state = getComboSelectedText('cbo-state');
  var municipality = getComboSelectedText('cbo-municipality');
  var location = getComboSelectedText('cbo-location');
  var street = document.getElementById("txt-street").value;
  var externalNo = document.getElementById("txt-externalNo").value;
  var internalNo = document.getElementById("txt-internalNo").value;
  var zipCode = document.getElementById("txt-zipCode").value;

  var address = parseAddress(id, description, street, externalNo, internalNo, location, municipality, state, country, zipCode);

  return address;
}

function haveAddress(traderId) {

  var addresses = getAddresses(traderId);
  if (addresses.length == 0) {
    return -1;
  }

  return 0;
  }

function saveAddress() {

  if (validateAddress() == -1) {
    return;
  }

  var traderId = sessionStorage.getItem('traderId'); 
  var newAddress = loadAddress();
  var operation = sessionStorage.getItem('addressOperation');
  if (operation != 'update') {
    addAdress(traderId, newAddress);
    alert("La dirección se agregado con exito!!!!");
  } else {
    var addressIndex = sessionStorage.getItem("addressIndex");
    updAddress(traderId, addressIndex, newAddress);
    sessionStorage.removeItem('addressOperation');
  }
 
  clearNamesList();
  fillListNames();
  hideAddressForm()
  showTraderInfo(traderId);

  return;
}

function removeAddress() {

  var traderId = sessionStorage.getItem('traderId');
  if (haveAddress(traderId) == -1) {
    alert("No se puede eliminar la dirección, ya que no encontre direcciones para este Contacto!!!");
    return;
  }
  var addressIndex = sessionStorage.getItem("addressIndex");
  delAddress(traderId, addressIndex);
  alert("La dirección ha sido eliminada");
  clearNamesList();
  fillListNames();
  showTraderInfo(traderId);

  return;  
}

function setAddressForm(address) {
    
  document.getElementById("txt-description").value = address.description;  
  document.getElementById("txt-street").value = address.street;
  document.getElementById("txt-externalNo").value = address.externalNo;
  document.getElementById("txt-internalNo").value = address.internalNo;
  document.getElementById("txt-zipCode").value = address.zipCode;

  return;
}

function updateAddress() {

  var traderId = sessionStorage.getItem('traderId');
  if (haveAddress(traderId) == -1) {
    alert("No se puede actualizar la dirección, ya que no encontre direcciones para este Contacto!!!");
    return;
  }
  var addressIndex = sessionStorage.getItem("addressIndex");
  var addresses = getAddresses(traderId);
  showAddressForm();
  setAddressForm(addresses[addressIndex]);
  sessionStorage.setItem("addressOperation", 'update');

  return;
}

// Address Operations
