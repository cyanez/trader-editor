﻿
function parseAddress(id, description, street, externalNo, internalNo, location, municipality, state, country, zipCode) {
  var address = new Address(id, description, street, externalNo, internalNo, location, municipality, state, country, zipCode);
  return address
}

var Address = function (id, description, street, externalNo, internalNo, location, municipality, state, country, zipCode) {
  this.id = Math.floor((Math.random() * 10000) + 1); //ask how can geve id from server
  this.description = description;
  this.street = street;
  this.externalNo = externalNo;
  this.internalNo = internalNo;
  this.location = location;
  this.municipality = municipality;
  this.state = state;
  this.country = country;
  this.zipCode = zipCode;
 
};

function addAdress(traderId, newAddress) {

  var traderObj = parseJson('traders');
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {
      traderObj[index].trader.addresses.push(newAddress);
      convertJson('traders', traderObj);
    }
  }

  return;
}

function delAddress(traderId, addressIndex) {

  var traderObj = parseJson('traders');
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {
      traderObj[index].trader.addresses.splice(addressIndex, 1);
      convertJson('traders', traderObj);
    }
  }

  return;
}

function getAddresses(traderId) {

  var traderObj = parseJson('traders');
  var Addresses = new Array();
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {
      Addresses = traderObj[index].trader.addresses;
    }
  }

  return Addresses;
}

function delAddress(traderId, addressIndex) {

  var traderObj = parseJson('traders');
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {
      traderObj[index].trader.addresses.splice(addressIndex, 1);
      convertJson('traders',traderObj);
    }
  }

  return;
}

function updAddress(traderId, addressIndex, newAddress) {

  var traderObj = parseJson('traders');
  for (index = 0; index < traderObj.length; index++) {
    if (traderId == traderObj[index].trader.id) {

      traderObj[index].trader.addresses[addressIndex].description = newAddress.description;
      traderObj[index].trader.addresses[addressIndex].description = newAddress.description;
      traderObj[index].trader.addresses[addressIndex].street = newAddress.street;
      traderObj[index].trader.addresses[addressIndex].externalNo = newAddress.externalNo;
      traderObj[index].trader.addresses[addressIndex].internalNo = newAddress.internalNo;
      traderObj[index].trader.addresses[addressIndex].location = newAddress.location;
      traderObj[index].trader.addresses[addressIndex].municipality = newAddress.municipality;
      traderObj[index].trader.addresses[addressIndex].state = newAddress.state;
      traderObj[index].trader.addresses[addressIndex].country = newAddress.country;
      traderObj[index].trader.addresses[addressIndex].zipCode = newAddress.zipCode;

      convertJson('traders', traderObj);
    }
  }

  return;
}