﻿function showTraderForm() {

  var addTraderForm = document.getElementById("div-trader");
  if (addTraderForm.style.visibility != "visible") {
    addTraderForm.style.visibility = "visible";
  }
  cleanTraderForm();
  return;
}


function hideTraderForm() {

  var addTraderForm = document.getElementById("div-trader");
  if (addTraderForm.style.visibility == "visible") {
    addTraderForm.style.visibility = "hidden";
  }

  return;
}

function cleanTraderForm() {

  cleanTextBox("id");
  cleanTextBox("txt-fisrtName"); 
  cleanTextBox("txt-lastName");
  cleanTextBox("txt-secondLastName");
  cleanTextBox("cal-bornDate");
  cleanTextBox("cbo-gender");
  cleanTextBox("txt-email");
  cleanTextBox("txt-phone");
  cleanTextBox("txt-phone2");

  return;
}

function loadTrader() {

  var firstName = document.getElementById("txt-fisrtName").value;
  var lastName = document.getElementById("txt-lastName").value;
  var secondLastName = document.getElementById("txt-secondLastName").value;
  var bornDate = document.getElementById("cal-bornDate").value;
  var gender = document.getElementById("cbo-gender").value;
  var email = document.getElementById("txt-email").value;
  var phone = document.getElementById("txt-phone").value;
  var phone2 = document.getElementById("txt-phone2").value;
  trader = Parse(firstName, lastName, secondLastName, bornDate, gender, email, phone, phone2);

  return trader;
}

//General validation functions

function empity(value) {

  if (value.length == 0) {
    return -1;
  }

  return 0;
}

function validateString(value) {

  var stringExpresion = /^[A-Za-z.áéíóúÑñ ]{1,20}$/;
  if (!stringExpresion.test(value)) {
    return -1;
  }

  return 0;
}

function validateEmail(email) {

  var emailExpresion = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  if ((email.length != 0) && (!emailExpresion.test(email))) {
    alert("el coreo electrónico tiene un formato incorrecto");
    return -1;
  }

  return 0;
}

function validateNumber(value) {

  var phoneExpresion = /^[0-9]{10,15}$/;
  if ((value.length != 0) && (!phoneExpresion.test(value))) {
    return -1;
  }

  return 0;
}

//endGeneral validation functions

//validate Form
function validateTrader(trader) {

  if (empity(trader.firstName) == -1) {
    alert("Uppss no has escrito el nombre del Contacto!!!");
    return -1;
  }
  if (validateString(trader.firstName) == -1) {
    alert("El nombre del contacto tiene caracteres no validos!!!");
    return -1;
  }
  if (empity(trader.lastName) == -1) {
    alert("Uppss no has escrito el apeido paterno del Contacto!!!");
    return -1;
  }
  if (validateString(trader.lastName) == -1) {
    alert("El apeido paterno del contacto del contacto tiene caracteres no validos!!!");
    return -1;
  }
  if (trader.bornDate.length == 0) {
    trader.bornDate = "1900-01-01";
  }
  if (empity(trader.gender)) {
    alert("Upss no has seleccionado el genero del Contacto!!!");
    return -1;
  }

  if (validateEmail(trader.email)) {
    return -1;
  }
  if (validateNumber(trader.phone)) {
    alert("Upps el número de teléfono debe contener 10 digitos numéricos")
    return -1;
  }
  if (validateNumber(trader.phone2)) {
    alert("Upps el número de celular debe contener 10 digitos numéricos")
    return -1;
  }

  return 0;
}

//End validate Form

//form operations

function saveTrader() {
    
  var trader = loadTrader();    
  if (validateTrader(trader) == -1) {
    return;
  }

  var traderAction = sessionStorage.getItem('traderOperation');
  if (traderAction != 'update') {    
    addTrader(trader);
    alert("El contacto: " + trader.fullName + " se a agragado!!!");
  } else {   
    var traderId = sessionStorage.getItem('traderId');    
    updTrader(traderId, trader);
    sessionStorage.removeItem('traderOperation');
  }    
  
  hideTraderForm();
  clearNamesList();
  fillListNames();
  hideTableInfo();
  
  return;
}

function removeTrader() {
  
  var traderId = sessionStorage.getItem('traderId');  
  if (delTrader(traderId) == -1) {
    alert("se presento algún inconveneinte y no fue posible eliminar el contacto!!!");
    return;
  }
  alert("El contacto  se ha eliminado ");
  hideTableInfo();
  clearNamesList();
  fillListNames();

  return;
}

function setFormValues(traderId) {

  var traderObj = getTraderById(traderId);
  showTraderForm();
  document.getElementById("id").value = traderObj.trader.id;
  document.getElementById("txt-fisrtName").value = traderObj.trader.firstName;
  document.getElementById("txt-lastName").value = traderObj.trader.lastName;
  document.getElementById("txt-secondLastName").value = traderObj.trader.secondLastName;
  document.getElementById("cal-bornDate").value = traderObj.trader.bornDate;
  document.getElementById("cbo-gender").value = traderObj.trader.gender;
  document.getElementById("txt-email").value = traderObj.trader.email;
  document.getElementById("txt-phone").value = traderObj.trader.phone;
  document.getElementById("txt-phone2").value = traderObj.trader.phone2;
 
  sessionStorage.setItem("isUpdate", 'true');

  return;
}

function updateTrader() {

  var traderId = sessionStorage.getItem('traderId'); 
  setFormValues(traderId); 
  sessionStorage.setItem("traderOperation", 'update'); 

  return;
}

//end Form Opearations