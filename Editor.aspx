﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Editor.aspx.cs" Inherits="Editor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:.:Editor de Contactos </title>     

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/addressForm.js"></script>
    <script type="text/javascript" src="js/traderForm.js"></script>
    <script type="text/javascript" src="js/address.js" ></script>
    <script type="text/javascript" src="js/trader.js"></script>
    <script type="text/javascript" src="js/externalFilesLoader.js"></script>
    <script type="text/javascript" src="js/editor.js" ></script>

    <link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
</head>
<body>
    <form id="traderEditor" runat="server">  

    <div class="header-bar"id="div-toolbar">
       <input type="text" class="header-bar" id="txt-name" placeholder="Busca personas o empresas " list="list-names"/>  
       <button type="button" class="header-bar" id="btn-search" onclick="searchTrader()"></button>
       <button type="button" class="header-bar" id="btn-add" onclick=" showTraderForm();"></button>  
        <datalist id="list-names">
         <%--  --%>
      </datalist> 
    </div>
       <div class="header-bar" id="table-container">
        <table class="header-bar" id="table-info">
          <tbody>
             <tr class='header-row'>
                    <td>
                        <div> 
                            Información General
                            <button type='button' class='left' id='btn-delTrader' onclick='removeTrader();'></button>
                            <button type='button' class='left' id='btn-updTrader' onclick='updateTrader();'></button>
                        </div>  
                      </td>
              </tr>
              <tr>
                <td>
                  <div id="div-generalInfo">

                  </div>
                </td>
              </tr>
              <tr class='header-row'>
                <td>
                  <div>  
                    Direcciones 
                    <button type='button' class='left' id='btn-delAddress' onclick='removeAddress();'></button>
                    <button type='button' class='left' id='btn-updAddress' onclick='updateAddress();'></button>
                    <button type='button' class='left' id='btn-addAddress' onclick='showAddressForm();'></button>
                  </div>  
                  </td>
              </tr>
              <tr>

                <td>
                  <div id="div-addressesInfo">

                  </div>
                </td>

              </tr>
          </tbody>
        </table>        
      </div> 

       <div id="div-trader">
   
       <label>Agregar Contactos</label><hr />       
       <label>Id:</label>
       <input type="text" id="id" readonly="" /> <br/>
       <label>Nombre: </label>
       <input type = "text" id = "txt-fisrtName" /> <br />
       <label>Apeido Paterno: </label>
       <input type = "text" id = "txt-lastName" /> <br />   
       <label>Apeido Materno:</label>
       <input type = "text" id = "txt-secondLastName" />  <br />   
       <label> Fecha de Nacimineto: </label>
       <input type = "date" id = "cal-bornDate" /> <br />      
       <label>Genero:</label>       
       <select id="cbo-gender">
         <option value ="">Selecciona genero</option>
         <option value="Female">Femenino</option>
         <option value="Male">Masculino</option>
         <option value="Otro">Otro</option>
       </select>       <br />
       <label>Correo eléctronico:</label>
       <input type= "email" id = "txt-email"  placeholder = "correo@servidor.com"/> <br />
       <label>Teléfono:</label>
       <input type="text" id = "txt-phone" placeholder="5558988938"/> <br />
       <label>Celular:</label>
       <input type="text" id = "txt-phone2" placeholder="5556667778"/><br /><hr />
       <button type="button" onclick="saveTrader();">Guardar</button>               
       <button type="button" onclick=" hideTraderForm();">Cancelar</button>       
    </div>

    <div id="div-address">
       <label>Agregar Domicilios: </label> <hr />
        <label>Descripción: </label>
        <input type="text" id="txt-description"  placeholder = " Pricipal/Trabajo "/> <br />        
        <label>País: </label> 
        <select id="cbo-country" onchange=" fillStatesComboBox('cbo-country');">
          <option value="-1">Elige un país</option>
        </select><br />   
        <label>Estado:</label>
        <select id="cbo-state" onchange="fillMuncipalitiesComboBox();">
           <option value="-1">Elige un estado</option>
        </select><br />          
        <label>Delegación o Municipio: </label> 
        <select id="cbo-municipality" onchange="fillLocationsComboBox();">
           <option value="-1">Elige un municipio</option>
        </select><br />     
        <label>Localidad: </label> 
         <select id="cbo-location" onchange="">
            <option value="-1">Elige una localidad</option>
         </select><br />  
        <label>Calle :</label>
        <input type="text" id = "txt-street" /> <br />
        <label>Número exterior: </label>
        <input type="text" id="txt-externalNo" /><br />  
        <label>Número interior: </label>
        <input type="text" id="txt-internalNo" /> <br />        
        <label>Código Postal: </label>
        <input type="text"  id="txt-zipCode" /> <br /> 
        <hr />
        <button type="button" onclick="saveAddress();">Guardar</button>               
        <button type="button" onclick="hideAddressForm();">Cancelar</button>
    </div>

    </form>
</body>
</html>
